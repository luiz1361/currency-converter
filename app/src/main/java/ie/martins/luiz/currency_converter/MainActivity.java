package ie.martins.luiz.currency_converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Defining the selectedCurrency variable which will represent the current value on the spinner
    private String selectedCurrency;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //--------Pulling the currency rates and currency extended names arrays from the XML resources--------
        final String[] str_ar_list_currency_extended_names = getResources().getStringArray(R.array.str_ar_currency_extended_name);
        final String[] str_ar_list_currency_rates = getResources().getStringArray(R.array.str_ar_currency_rates);

        //--------Defining the editText's initial value--------
        final EditText editText = (EditText) findViewById(R.id.editText);
        editText.setText(R.string.str_initial_value);

        //--------Pulling the Spinner from the XML--------
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        //--------Pulling the currency short name array from the XML resources and linking to the
        //Array adapter to be used with the spinner--------
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.str_ar_currency_short_name,
                android.R.layout.simple_spinner_item);

        //--------Specifying the layout that will be used on the list of choices when the spinner is clicked--------
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //--------Defining the Convert button--------
        final Button button = (Button) findViewById(R.id.button);

        //--------Initial List View--------
        final List<String> joined = new ArrayList<>();
        joined.add(str_ar_list_currency_rates[0] + " " + str_ar_list_currency_extended_names[0]);
        joined.add(str_ar_list_currency_rates[1] + " " + str_ar_list_currency_extended_names[1]);
        joined.add(str_ar_list_currency_rates[2] + " " + str_ar_list_currency_extended_names[2]);
        joined.add(str_ar_list_currency_rates[3] + " " + str_ar_list_currency_extended_names[3]);
        joined.add(str_ar_list_currency_rates[4] + " " + str_ar_list_currency_extended_names[4]);
        joined.add(str_ar_list_currency_rates[5] + " " + str_ar_list_currency_extended_names[5]);
        final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
        final ListView lv_main_list = (ListView) findViewById(R.id.listView);
        lv_main_list.setAdapter(str_ar_ada_joined);

        //--------Setting the Spinner's adapter--------
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //--------Method that will be called when an item has been selected--------
            public void onItemSelected(AdapterView<?> parent, View view, final int pos, long
                    id) {
                //--------Saving the selected for future use on the OnClickListener--------
                selectedCurrency = spinner.getSelectedItem().toString();
            }
            //--------Overridden method that will be called when no item has been selected in the spinner--------
            public void onNothingSelected(AdapterView<?> parent) {
                //--------Here is where we would react if no item was selected--------
            }
        });

                //--------Defining the actions that will have if we have a match in a condition when we click on the button--------
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //--------If you do not enter a value or just .--------
                        if (editText.getText().toString().equals("") || (editText.getText().toString().equals("."))) {
                            Toast.makeText(MainActivity.this, "You did not enter a valid value", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //--------If the button was clicked and the Spinner value is EUR--------
                        if ( selectedCurrency.equals("EUR") ) {
                            //--------Saving the float value of the editText--------
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            //--------Refreshing the List View based on the selectedCurrency doing the following calculations--------
                            final List<String> joined = new ArrayList<>();
                            joined.add(value + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value * Float.parseFloat(str_ar_list_currency_rates[1])  + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value * Float.parseFloat(str_ar_list_currency_rates[2])  + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value * Float.parseFloat(str_ar_list_currency_rates[3])  + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value * Float.parseFloat(str_ar_list_currency_rates[4])  + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value * Float.parseFloat(str_ar_list_currency_rates[5])  + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }

                        if ( selectedCurrency.equals("GBP") ) {
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            final List<String> joined = new ArrayList<>();
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[1]) + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[1]) * Float.parseFloat(str_ar_list_currency_rates[2]) + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[1]) * Float.parseFloat(str_ar_list_currency_rates[3]) + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[1]) * Float.parseFloat(str_ar_list_currency_rates[4]) + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[1]) * Float.parseFloat(str_ar_list_currency_rates[5]) + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }

                        if ( selectedCurrency.equals("USD") ) {
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            final List<String> joined = new ArrayList<>();
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[2]) + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[2]) * Float.parseFloat(str_ar_list_currency_rates[1]) + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[2]) * Float.parseFloat(str_ar_list_currency_rates[3]) + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[2]) * Float.parseFloat(str_ar_list_currency_rates[4]) + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[2]) * Float.parseFloat(str_ar_list_currency_rates[5]) + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }

                        if ( selectedCurrency.equals("JPY") ) {
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            final List<String> joined = new ArrayList<>();
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[3]) + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[3]) * Float.parseFloat(str_ar_list_currency_rates[1]) + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[3]) * Float.parseFloat(str_ar_list_currency_rates[2]) + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[3]) * Float.parseFloat(str_ar_list_currency_rates[4]) + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[3]) * Float.parseFloat(str_ar_list_currency_rates[5]) + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }

                        if ( selectedCurrency.equals("AUD") ) {
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            final List<String> joined = new ArrayList<>();
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[4]) + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[4]) * Float.parseFloat(str_ar_list_currency_rates[1]) + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[4]) * Float.parseFloat(str_ar_list_currency_rates[2]) + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[4]) * Float.parseFloat(str_ar_list_currency_rates[3]) + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[4]) * Float.parseFloat(str_ar_list_currency_rates[5]) + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }

                        if ( selectedCurrency.equals("CAD") ) {
                            Float value;
                            value = Float.parseFloat(editText.getText().toString());
                            final List<String> joined = new ArrayList<>();
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[5]) + " " + str_ar_list_currency_extended_names[0]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[5]) * Float.parseFloat(str_ar_list_currency_rates[1]) + " " + str_ar_list_currency_extended_names[1]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[5]) * Float.parseFloat(str_ar_list_currency_rates[2]) + " " + str_ar_list_currency_extended_names[2]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[5]) * Float.parseFloat(str_ar_list_currency_rates[3]) + " " + str_ar_list_currency_extended_names[3]);
                            joined.add(value / Float.parseFloat(str_ar_list_currency_rates[5]) * Float.parseFloat(str_ar_list_currency_rates[4]) + " " + str_ar_list_currency_extended_names[4]);
                            joined.add(value + " " + str_ar_list_currency_extended_names[5]);
                            final ArrayAdapter<String> str_ar_ada_joined = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, joined);
                            final ListView lv_main_list = (ListView) findViewById(R.id.listView);
                            lv_main_list.setAdapter(str_ar_ada_joined);
                        }
                    }
                });
            }

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.menu_main, menu);
                return true;
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // Handle action bar item clicks here. The action bar will
                // automatically handle clicks on the Home/Up button, so long
                // as you specify a parent activity in AndroidManifest.xml.
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_settings) {
                    return true;
                }

                return super.onOptionsItemSelected(item);
            }
        }
